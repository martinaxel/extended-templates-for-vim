"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @file vimrc
" @author Axel Martin <axel.martin@eisti.fr>
" @brief Définition du fichier de configuration de Vim
" @version 0.1
" @date 2014-10-22
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype indent plugin on " Active la détection des types de fichiers, des plugins et de l'indentation selon le type de fichier.
syntax on                 " Activation de la coloration syntaxique

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Content Must Have
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nowrap                " Ne remet pas à la ligne lorsque la ligne est trop grande
set hidden
set wildmenu
set showcmd


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Options de recherche
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ignorecase            " Ignorer la casse en recherche
set smartcase             " Ignore la casse si le patern est en lower case, sinon est sensible à la casse
set hlsearch              " Mise en valeur des matchs d'une recherche
set incsearch             " Affiche les correspondances de la recherche durant
                          " l'écriture de la recherche


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Readability
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number                " Affichage des numéros des lignes
set background=dark       " Couleurs adaptées à un background noir


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indentation
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set autoindent            " Indentation automatique
set copyindent
set shiftwidth=4          " Nombre d'espace pour autoindent
set tabstop=2             " Une tabulation est 4 espaces
set shiftround            " Utilisation d'un multiple de shiftwidth pour `<`et `>`
set softtabstop=4
set expandtab


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Other configurations
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set title                 " Change le nom du terminal
set noerrorbells          " Enlève le buzzer
set wildignore=*.swp,*.bak,*.pyc,*.class
let g:tex_flavor = "latex"
let g:DefaultAuthor = "Axel Martin"
let g:DefaultEmail  = "<axel.martin@eisti.fr>"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mapping section
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <C-Right> :tabnext<CR>
map <C-Left> :tabprev<CR>
