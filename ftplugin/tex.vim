" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1


" Comments related definitions
if !exists("g:FTLatex_longSeparator")
  let g:FTLatex_longSeparator = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
endif
if !exists("g:FTLatex_beforeTag")
  let g:FTLatex_beforeTag = "% "
endif
if !exists("g:FTLatex_fileNameTag")
  let g:FTLatex_fileNameTag = "name:         | "
endif
if !exists("g:FTLatex_authorTag")
  let g:FTLatex_authorTag   = "@uthor:       | "
endif
if !exists("g:FTLatex_titleTag")
  let g:FTLatex_titleTag    = "title:        | "
endif
if !exists("g:FTLatex_licenceTag")
  let g:FTLatex_licenceTag  = "licence:      | "
endif
if !exists("g:FTLatex_moreTag")
  let g:FTLatex_moreTag     = "more:         | "
endif
if !exists("g:FTLatex_licenceValue")
  let g:FTLatex_licenceValue = "free"
endif

" Latex related definitions
if !exists("g:FTLatex_documentClass")
  let g:FTLatex_documentClass = "article"
endif

if !exists("g:FTLatex_packages")
  let g:FTLatex_packages = {'babel': 'french', 'inputenc': 'utf8', 'amsfonts': '', 'mathtools': '', 'fontenc': 'T1'}
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @func LatexHeaderStart
" @brief Affichage de l'entête Latex d'un document automatiquement
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>LatexHeaderStart()

  " Checking author
  if !exists("g:DefaultAuthor")
    let g:DefaultAuthor = input("Enter name of the author(s) : ")
  endif

  let l:LatexTitle = input("Enter title of your document : ")
  let l:FileName   = expand("%:t")
  let l:Date       = strftime("%Y-%m-%d")

  " Begin to write head comments
  exec "normal O".g:FTLatex_longSeparator
  exec "normal o".g:FTLatex_beforeTag.g:FTLatex_fileNameTag.l:FileName
  exec "normal o".g:FTLatex_beforeTag.g:FTLatex_authorTag.g:DefaultAuthor." ".g:DefaultEmail
  exec "normal o".g:FTLatex_beforeTag.g:FTLatex_titleTag.l:LatexTitle
  exec "normal o".g:FTLatex_beforeTag.g:FTLatex_licenceTag.g:FTLatex_licenceValue
  exec "normal o".g:FTLatex_beforeTag.g:FTLatex_moreTag
  exec "normal o".g:FTLatex_longSeparator
  exec "normal o"

  exec "normal o\\documentclass{".g:FTLatex_documentClass."}"
  exec "normal o"
  exec "normal o"

  " Begin to write package inclusion
  for package in keys(g:FTLatex_packages)
    if (g:FTLatex_packages[package] != "")
      exec "normal o\\usepackage[".g:FTLatex_packages[package]."]{".package."}"
    else
      exec "normal o\\usepackage{".package."}"
    endif
  endfor
  exec "normal o"
  exec "normal o"

  " Title and author commands
  exec "normal o\\title{".l:LatexTitle."}"
  exec "normal o\\author{".g:DefaultAuthor."}"
  exec "normal o"
  exec "normal o"

  " Starting document
  exec "normal o\\begin{document}"
  exec "normal o"
  exec "normal o\\maketitle"
  exec "normal o\\tableofcontents"
  exec "normal o"
  exec "normal o\\newpage"
  exec "normal o"
  exec "normal o"
  exec "normal o"
  mark d
  exec "normal o"
  exec "normal o"
  exec "normal o\\end{document}"

  exec "normal `d"
  startinsert!
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Génération d'un exemple en latex
" @version 0.1
" @date 2014-10-20
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>TexExample()

  exec "normal o\\begin{center}"
  exec "normal o\\fbox{"
  exec "normal o\\begin{minipage}{0.9\\textwidth}"
  exec "normal o\\textbf{Exemple :}"
  exec "normal o"
  exec "normal o"
  mark d
  exec "normal o\\end{minipage}"
  exec "normal o}"
  exec "normal o\\end{center}"

  exec "normal `d"
  startinsert!
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @func PDFLatex
" @brief Compilation latex d'un fichier en passant par l'utilitaire pdflatex
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>PDFLatex()

  " Getting filename
  let l:FileName = expand("%:p")

  " Executing pdflatexe
  let l:result = system("pdflatex -halt-on-error " . l:FileName)
  if v:shell_error
    echohl WarningMsg
    echo "PDFLatex failed. Most recent traceback :"
    echohl None
    let l:errors = split(l:result, '\n')[-15:-2]
    for line in l:errors
      echo line
    endfor
    return 1
  endif

  return 0
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @func PDFLatexAndView
" @brief Compilation latex d'un fichier en passant par l'utilitaire pdflatex
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>PDFLatexAndView()

  " Getting filename
  let l:PdfFileName = expand("%:r") . ".pdf"

  " Executing pdflatex
  if (! <SID>PDFLatex() )
    exec "silent !evince " . l:PdfFileName . " 2> /dev/null & "
  endif

  " Fixing window issue
  exec "redraw!"
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Définition des shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command! -nargs=0 TexStart :call <SID>LatexHeaderStart()
command! -nargs=0 Pdflatex :call <SID>PDFLatex()
command! -nargs=0 PdflatexAndView :call <SID>PDFLatexAndView()
command! -nargs=0 TexExample :call <SID>TexExample()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mapping des fonctions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F6> :Pdflatex<CR>
map <F7> :PdflatexAndView<CR>
