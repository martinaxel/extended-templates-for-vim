"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @file LoadEnvironment.vim
" @author Axel Martin <axel.martin@eisti.fr>
" @brief Shortcuts for loading an environement (project, vimscript editing...)
" @version 0.1
" @date 2014-10-22
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Définition de constantes
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists("g:EnvironementConfigFileName")
  let g:EnvironementConfigFileName = ".vimcfg"
endif

if !exists("g:EnvironementDefaultConfigFile")
  let g:EnvironementDefaultConfigFile = "~/.vim/vim_global_cfg"
endif

if !exists("g:EnvironementLoadDefaultIfLocalNotFound")
  let g:EnvironementLoadDefaultIfLocalNotFound = 0
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Recherche un fichier de définition d'environement local
" @version 0.1
" @date 2014-10-22
" 
" @return le fichier d'environement local ou le fichier d'environement global
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>FindLocalEnvironement()

  let l:cwd = split(expand("%:p:h"), "/")
  let l:home = split(expand("~"), "/")


  if (l:home != l:cwd[0:len(l:home)-1])
    return g:EnvironementDefaultConfigFile
  endif
  
  while (l:cwd != l:home && empty(glob("/" . join(l:cwd, "/") . "/" . g:EnvironementConfigFileName)))
    let l:cwd = l:cwd[0:-2]
  endwhile

  if (l:cwd == l:home)
    return g:EnvironementDefaultConfigFile
  endif

  return "/" . join(l:cwd, "/") . "/" . g:EnvironementConfigFileName
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief sauvegarde d'une session dans un fichier de sauvegarde.
" @version 0.1
" @date 2014-10-22
" 
" @rem si la fonction est lancée sans argument, elle ira chercher un fichier de
"      configuration locale. Sinon, elle prendra l'adresse
"      `~/.vim/session-save/$name.vim`
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>SaveEnvironement( ... )

  if !exists("a:0")
    let l:filepath = <SID>FindLocalEnvironement()
    if (l:filepath == g:EnvironementDefaultConfigFile)
      echohl WarningMsg
      echo "No local environement file found."
      echohl None
      return
    endif
  else
    let l:filepath = "~/.vim/session-save/".a:0.".vim"
  endif

  exec "mksession! " . l:filepath

endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief charge un environement à partir d'un fichier de configuration
" @version 0.1
" @date 2014-10-22
" 
" @param filepath adresse du fichier de configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>LoadEnvironement( filepath )

  if empty(glob(a:filepath))
    echohl WarningMsg
    echo "Environement file " . a:filepath . " not found."
    echohl None
    return
  endif

  exec "source " . a:filepath

endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Création des shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command! -nargs=1 LoadEnvironement :call <SID>LoadEnvironement(<f-args>)
command! -nargs=? SaveEnvironement :call <SID>SaveEnvironement(<f-args>)


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Script opéré à chaque lancement
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let s:CommandLine = system('tr "\0" " " </proc/' . getpid() . '/cmdline')
if (match(s:CommandLine, '^[[:blank:]]*vim[[:blank:]]*$') == 0)
  let s:Env = <SID>FindLocalEnvironement()
  if (s:Env == g:EnvironementDefaultConfigFile && g:EnvironementLoadDefaultIfLocalNotFound || s:Env != g:EnvironementDefaultConfigFile)
    call <SID>LoadEnvironement(s:Env)
  endif
endif

