" Standard constants definition
if !exists("g:Doxygen_briefTag")
  let g:Doxygen_briefTag = "@brief "
endif
if !exists("g:Doxygen_templateParamTag")
  let g:Doxygen_templateParamTag = "@tparam "
endif
if !exists("g:Doxygen_paramTag")
  let g:Doxygen_paramTag = "@param "
endif
if !exists("g:Doxygen_returnTag")
  let g:Doxygen_returnTag = "@return "
endif
if !exists("g:Doxygen_throwTag")
  let g:Doxygen_throwTag = "@throw "
endif
if !exists("g:Doxygen_fileTag")
  let g:Doxygen_fileTag = "@file "
endif
if !exists("g:Doxygen_authorTag")
  let g:Doxygen_authorTag = "@author "
endif
if !exists("g:Doxygen_dateTag")
  let g:Doxygen_dateTag = "@date "
endif
if !exists("g:Doxygen_versionTag")
  let g:Doxygen_versionTag = "@version "
endif
if !exists("g:Doxygen_nameTag")
  let g:Doxygen_nameTag = "@name "
endif
if !exists("g:Doxygen_classTag")
  let g:Doxygen_classTag = "@class "
endif

" Licence
let s:licenceTag = "Copyright (C) {{date}} {{author}} \<enter>\<enter>"
let s:licenceTag = s:licenceTag . "This program is free software; you can redistribute it and/or\<enter>"
let s:licenceTag = s:licenceTag . "modify it under the terms of the GNU General Public Licence\<enter>"
let s:licenceTag = s:licenceTag . "as published by the Free Software Foundation; eitther version 2\<enter>"
let s:licenceTag = s:licenceTag . "of the Licence, or (at your option) any later version.\<enter>\<enter>"
let s:licenceTag = s:licenceTag . "This program is distributed in the hopee that it will be useful,\<enter>"
let s:licenceTag = s:licenceTag . "but WITHOUT ANY WARRANTTY; without even the implied warranty of\<enter>"
let s:licenceTag = s:licenceTag . "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\<enter>"
let s:licenceTag = s:licenceTag . "GNU General Public Licence for more details.\<enter>\<enter>"
let s:licenceTag = s:licenceTag . "You should have received a copy of tthe GNU General Public Licence\<enter>"
let s:licenceTag = s:licenceTag . "along with this program; if not, write to the Free Software\<enter>"
let s:licenceTag = s:licenceTag . "Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.\<enter>"

" Parameters for Doxygen
if !exists("g:Doxygen_maxFunctionProtoLines")
  let g:Doxygen_maxFunctionProtoLines = 10
endif

if !exists("g:Doxygen_languageInitialize")
  let g:Doxygen_languageInitialize = {}
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Génération d'une licence logicielle (GPL)
" @version 1.0
" @date 2014-10-17
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>DoxygenLicenceGenFunc()
  call s:InitializeDoxygen()

  " Test the global author name variable
  if !exists("g:DefaultAuthor")
    let g:DefaultAuthor = input("Enter name of the author : ")
  endif

  let l:date = strftime("%Y")
  let s:licence = substitute(s:licenceTag, "{{date}}", l:date, "g")
  let s:licence = substitute(s:licence, "{{author}}", g:DefaultAuthor, "g")
  let s:licence = substitute(s:licence, "\<enter>", "\<enter>" . g:Doxygen_interCommentTag, "g")

  if (g:Doxygen_startCommentBlock != "")
    exec "normal O".g:Doxygen_startCommentTag
  endif
  exec "normal o".g:Doxygen_interCommentTag
  exec "normal A".s:licence
  if (g:Doxygen_endCommentBlock != "")
    exec "normal o".g:Doxygen_endCommentTag
  endif

  call s:RestoreParameters()
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Génération de commentaires d'en-tête de fichier
" @version 1.0
" @date 2014-10-17
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>DoxygenAuthorGenFunc()
  call s:InitializeDoxygen()

  " Test the global author name variable
  if !exists("g:DefaultAuthor")
    let g:DefaultAuthtor = input("Enter name of the author : ")
  endif

  if !exists("g:DefaultEmail")
    let g:DefaultEmail = input("Enter the mail of the author : ")
  endif

  if !exists("g:Doxygen_versionString")
    let g:Doxygen_versionString = input("Enter version string : ")
  endif

  let l:fileName = expand("%:t")
  let l:date = strftime("%Y-%m-%d")

  exec "normal O".g:Doxygen_startCommentTag
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_fileTag.l:fileName
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_authorTag.g:DefaultAuthor." ".g:DefaultEmail
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_briefTag
  mark d
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_versionTag.g:Doxygen_versionString
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_dateTag.l:date
  if (g:Doxygen_endCommentTag != "")
    exec "normal o".g:Doxygen_endCommentTag
  endif

  exec "normal `d"

  call s:RestoreParameters()
  startinsert!
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Affiche un message d'erreur
" @version 1.0
" @date 2014-10-17
" 
" @param message message à afficher
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! s:ErrorMessage(message)
  echohl WarningMsg
  echo a:message
  echohl None
  return
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Initialisation de Doxygen en fonction du langage à documenter
" @version 1.0
" @date 2014-10-17
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>InitializeDoxygen()
  if !exists("g:Doxygen_languageInitialize")
    call s:ErrorMessage("No language database initialisation for Doxygen")
    return
  endif

  if !has_key(g:Doxygen_languageInitialize, &filetype)
    call s:ErrorMessage("Language initialisation database has no key " . &filetype)
    return
  endif

  call g:Doxygen_languageInitialize[&filetype]()

  " Backup des paramètres d'écriture (indentation, expension...)
  let s:commentsBackup = &comments
  let s:cinoptionsBackup = &cinoptions
  let s:timeoutlenBackup = &timeoutlen
  " Mise en place des paramètres d'écriture simplifiés
  let &comments = ""
  let &cinoptions = "c1C1"
  let &timeoutlen = 0
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Restoration des paramètres une fois les générations faites
" @version 1.0
" @date 2014-10-17
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>RestoreParameters()
  let &comments = s:commentsBackup
  let &cinoptions = s:cinoptionsBackup
  let &timeoutlen = s:timeoutlenBackup
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Génération de commentaire automatiques pour des éléments de programmes
" @version 1.0
" @date 2014-10-17
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>DoxygenCommentFuncGen()
  call s:InitializeDoxygen()

  mark d

  " Dans un premier temps, on vérifie qu'il y a bien quelque chose à
  " documenter dans les 3 premières lignes sous le curseur
  let l:lineBuffer = getline(line('.'))
  let l:count = 1
  while (match(l:lineBuffer, "^[[:blank:]]*$") != -1 && l:count < 4)
    exec "normal j"
    let l:lineBuffer = l:lineBuffer.' '.getline(line("."))
    let l:count = l:count + 1
  endwhile
  if (match(l:lineBuffer, "^[[:blank:]]*$") != -1)
    call s:ErrorMessage("Nothing to document here !")
    exec "normal `d"
    return
  endif
  mark d

  " Maintenant, on essaye de déterminer la chaine de définition de la fonction
  let l:count = 0
  while (l:count < g:Doxygen_maxFunctionProtoLines)

    " Vérification du matching de la definition de la fonction
    if (match(l:lineBuffer, g:Doxygen_endDefinitionRegexp) != -1)
      break
    endif

    let l:count = l:count + 1
    exec "normal j"

    " Suppression des commentaires éventuels dans la fonction
    if (g:Doxygen_inlineCommentRegexp != "")
      let l:currentLine = substitute(getline(line(".")), g:Doxygen_inlineCommentRegexp, '', '')
    else
      let l:currentLine = getline(line("."))
    endif
    if (g:Doxygen_multilineCommentRegexp != "")
      let l:lineBuffer  = substitute(l:lineBuffer . l:currentLine, g:Doxygen_multilineCommentRegexp, '', '')
    else
      let l:lineBuffer  = l:lineBuffer . l:currentLine
    endif
  endwhile

  if (l:count >= g:Doxygen_maxFunctionProtoLines)
    call s:ErrorMessage("Nothing to document here !")
    exec "normal `d"
    return
  endif

  " Démarage du parsing
  let l:type = ""
  for key in keys(g:Doxygen_typesRegexp)
    if (match(l:lineBuffer, g:Doxygen_typesRegexp[key]) != -1)
      let l:type = key
      break
    endif
  endfor
  
  if l:type == ""
    call s:ErrorMessage("Unknown type definition")
    exec "normal `d"
    return
  endif

  " Récupération des données supplémentaires selon les structures
  let l:type_keys = {}
  for key in keys(g:Doxygen_typesParticularity[l:type])
    let l:Operation = g:Doxygen_typesParticularity[l:type][key]
    if type(l:Operation) == type("") " type string
      if g:Doxygen_typesParticularity[l:type][key] ==  ''
        let l:type_keys[key] = ""
      else
        let l:type_keys[key] = matchstr(l:lineBuffer, l:Operation)
      endif
    elseif type(l:Operation) == type(function("tr")) " type function 
      let l:type_keys[key] = l:Operation(l:lineBuffer)
    endif
    unlet l:Operation
  endfor

  " Ajout d'informations supplémentaires
  if !exists("g:DefaultAuthor")
    let g:DefaultAuthor = input("Enter name of the author :")
  endif
  if !exists("g:Doxygen_versionString")
    let g:Doxygen_versionString = input("Enter version string : ")
  endif
  let l:date = strftime("%Y-%m-%d")

  " Affichage du résultat
  exec "normal `d"
  exec "normal O".g:Doxygen_startCommentTag
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_authorTag.g:DefaultAuthor
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_briefTag
  mark d
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_versionTag.g:Doxygen_versionString
  exec "normal o".g:Doxygen_interCommentTag.g:Doxygen_dateTag.l:date
  exec "normal o".g:Doxygen_interCommentTag
  for key in keys(l:type_keys)
    if type(l:type_keys[key]) == type("")
      exec "normal o".g:Doxygen_interCommentTag.key.l:type_keys[key]
    elseif type(l:type_keys[key]) == type([])
      for element in l:type_keys[key]
        exec "normal o".g:Doxygen_interCommentTag.key.element
      endfor
    endif
  endfor
  
  exec "normal o".g:Doxygen_endCommentTag
  exec "normal `d"

  call s:RestoreParameters()
  startinsert!
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Création des shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command! -nargs=0 DoxLic :call <SID>DoxygenLicenceGenFunc()
command! -nargs=0 DoxAuthor :call <SID>DoxygenAuthorGenFunc()
command! -nargs=0 Dox :call <SID>DoxygenCommentFuncGen()
