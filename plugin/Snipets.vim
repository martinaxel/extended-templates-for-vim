"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @file Snipets.vim
" @author Axel Martin <axel.martin@eisti.fr>
" @brief Enregistrement et génération de code à partir de Snipets
" @version 0.1
" @date 2014-10-23
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Quelques définitions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists("g:SnipetsFolder")
  let g:SnipetsFolder = expand("~") . "/.vim/snipets/"
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Définition d'une liste de snipets disponibles avec leurs description
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists("s:AvaibleSnipets")
    let s:AvaibleSnipets = {}

    let s:Snipets = split(glob(g:SnipetsFolder . "*"), "\n")
    for snipet in s:Snipets
        let s:AvaibleSnipets[split(snipet, "/")[-1]] = readfile(snipet)[0][2:-1]
    endfor
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author xolox
" @brief Récupération d'une selection visuelle dans vim
" @date 2014-10-23
" 
" @return La selection visuelle dans vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! s:get_visual_selection()
  let [lnum1, col1] = getpos("'<")[1:2]
  let [lnum2, col2] = getpos("'>")[1:2]
  let lines = getline(lnum1, lnum2)
  let lines[-1] = lines[-1][: col2 - (&selection == 'inclusive' ? 1 : 2)]
  let lines[0] = lines[0][col1 - 1:]
  return lines
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Enregistrement d'une selection dans vim dans un fichier annexe
" @version 0.1
" @date 2014-10-23
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>RegisterSnipet()

  let l:selection = s:get_visual_selection()
  if l:selection == []
    echohl WarningMsg
    echo "No selection found !"
    echohl None
    return
  endif

  let l:name = input("Enter a name for your snippet : ")
  let l:name = g:SnipetsFolder . l:name . "." . &filetype

  if !empty(glob(l:name))
    echohl WarningMsg
    echo "This snipet name is already used."
    echohl None
    return
  endif

  let l:desc = input("Enter a description for your snippet : ")
  let l:desc = '" ' . l:desc
  let l:selection = [l:desc] + l:selection

  call writefile(l:selection, l:name)
  return
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Récupère les snipets dont le nom correspond à un regexp et au filetype
" @version 0.1
" @date 2014-10-23
" 
" @param regexp regexp à tester sur les snipets et leur description (si nul, tous les snipets sont selectionnés
" @return une
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>GetSnippets( regexp )

  let l:output = {}

  for snipet in keys(s:AvaibleSnipets)
    if (match(snipet, '^.*\.' . &filetype) != -1)
      if (a:regexp == "")
        let l:output[snipet] = s:AvaibleSnipets[snipet]
      else
        if (match(snipet, a:regexp) >= 0 || match(s:AvaibleSnipets[snipet], a:regexp) >= 0)
          let l:output[snipet] = s:AvaibleSnipets[snipet]
        endif
      endif
    endif
  endfor

  return l:output

endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Recherche de snipets en correspondance avec le filetype du fichier courant
" @version 0.1
" @date 2014-10-23
" 
" @param regexp <optional> si spécifié, va effectuer une recherche sur la description et le nom du fichier
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>FindSnipet( ... )

  if (a:0 == 0)
    let l:snipets = <SID>GetSnippets( "" )
  else
    let l:snipets = <SID>GetSnippets( a:1 )
  endif

  for snipet in keys(l:snipets)
    echo snipet . "\t" . s:AvaibleSnipets[snipet]
  endfor
  
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Affichage d'un message d'erreur
" @version 0.1
" @date 2014-10-23
" 
" @param msg message à afficher
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! s:WarningMsg( msg )
  echohl WarningMsg
  echo a:msg
  echohl None
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" @author Axel Martin
" @brief Inclusion d'un snipet référencé
" @version 0.1
" @date 2014-10-23
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! <SID>IncludeSnipet( ... )

  " Recherche des snippets correspondants
  if (a:0 == 0)
    let l:snipets = <SID>GetSnippets( "" )
  else
    let l:snipets = <SID>GetSnippets( a:1 )
  endif

  " 
  if (len(l:snipets) == 0)
    call s:WarningMsg("No snipets found.")
    return
  elseif (len(l:snipets) == 1)
    let l:key = keys(l:snipets)[0]
  else

    echo "Multiple possibilitys :"
    let l:iter = 1
    for key in keys(l:snipets)
      echo l:iter . "\t" . key . "\t" . l:snipets[key]
      let l:iter += 1
    endfor
    let l:input = input("Choose your snipet (0 for exit) : ")

    if (l:input == 0)
      call s:WarningMsg("Error : bad input.")
      return
    elseif (l:input >= 1 && l:input <= len(l:snipets))
      let l:key = keys(l:snipets)[l:input - 1]
    else
      return
    endif
  endif

  for line in readfile(g:SnipetsFolder . l:key)[1:-1]
    let line = substitute(line, '^[[:blank:]]*', '', 'g')
    exec "normal o".line
  endfor
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Création des shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command! -nargs=0 RegisterSnipet :call <SID>RegisterSnipet()
command! -nargs=? FindSnipet :call <SID>FindSnipet(<f-args>)
command! -nargs=? IncludeSnipet :call <SID>IncludeSnipet(<f-args>)

